section .text
global _start

strlen:
    xor rdx, rdx           ; count of characters
    strlen_L0S:
        mov bl, [rax+rdx]  ; load next byte.
        cmp bl, 0          ; is it nullterm?
        je  strlen_L0E     ; if so exit

        inc rdx            ; otherwise increment count
        jmp strlen_L0S     ; (repeat)
    strlen_L0E:
        mov rax, rdx
        ret

puts:
    mov rax, [rbp-8]  ; "rbp" points to the saved "rbp" so we have
                      ; to offset by the size of the argument passed
                      ; to access it.
    call strlen

    mov [rsp-8], rax  ; store length of string, we store it in local
                      ; variable space, deliberately, to access it
                      ; through the caller.

    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    mov rsi, [rbp-8]
    syscall

    ret

_start:
    push rbp
    mov  rbp, rsp

    push message
    call puts     ; at the time when call is executed, "rip" points to
                  ; the next instruction so when the called function pops
                  ; the saved "rip" off the stack into "rip" back the
		  ; execution resumes from the next instruction.

    mov rax, 1
    mov rdi, 1
    mov rsi, message_reversed
    mov rdx, [rsp-16]  ; "rsp" now points to the start of the last
                       ; parameter passed into callee, next one is
                       ; the return address and then starts the area
                       ; where local variables are stored.
                       ;
                       ; first one is the size of the string passed.
    syscall

    ;; we don't follow the function call epilogue because this
    ;; returning to the caller anyways (there's no return addr),
    ;; thus we must terminate using syscall.
    ; mov rsp, rbp   ; discard the pushed values
    ; pop rbp        ; restore the base pointer

    mov rax, 60
    xor rdi, rdi
    syscall

section .data
message:          db "Hello World", 10, 0
message_reversed: db "Dlrow Olleh", 10, 0
