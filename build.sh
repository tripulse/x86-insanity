#!/bin/env bash
for file in *.s
do
	export outfile=${file%.*}

	nasm -f elf64 $file
	ld -m elf_x86_64 -o $outfile $outfile.o

	rm $outfile.o
done
